FROM python:3.7-slim

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY *.py ./
ENV PYTHONUNBUFFERED=1
CMD ["./app.py"]