#!/usr/bin/env python3

import logging
from queue import Queue
from threading import Thread

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, Bot, Update, MessageEntity, Message,
    InlineKeyboardMarkup, InlineKeyboardButton)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler, Dispatcher, Handler, CallbackContext)

from functools import wraps

import paho.mqtt.client as mqtt
import datetime

from os import getenv, environ

from lru_cache import LRUCache

import yaml

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

TOKEN = environ['TELEGRAM_TOKEN' ]

MQTT_SERVER = environ['MQTT_SERVER']
MQTT_PORT = getenv('MQTT_PORT', '1883'); MQTT_PORT = int(MQTT_PORT)
MQTT_SSL = getenv('MQTT_SSL', 'true'); MQTT_SSL = MQTT_SSL.lower() in ['true', '1',  'y', 'yes']

MQTT_USER = environ['MQTT_USER']
MQTT_PASSWORD = environ['MQTT_PASSWORD']

logger.info('MQTT: %s:%d' % (MQTT_SERVER, MQTT_PORT) )

mqtt_client = None

bot = None
last_command_sender = None

last_chats = LRUCache(5)

storage_file = getenv('STORAGE_FILE', 'storage.yml')
storage = None
ids = None

def restricted(func):
    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        user_id = update.effective_user.id
        global ids
        if len(ids)>0 and user_id not in ids:
            update.effective_message.reply_text('Not authorized')
            logger.warning(f'Unauthorized access denied for {user_id}')
            return
        return func(update, context, *args, **kwargs)
    return wrapped

def build_menu(item):
    root = {'id': '', 'text': 'Outside menu', 'items': (item,) }
    return build_menu_item(root)

def build_menu_item(item):
    # logger.info(f'processing {item}')
    if isinstance(item,dict):
        _id, text = item['id'], item['text']
        subitems = item.get('items', None)
        func = item.get('func', None)
    else:
        _id, text, t = item
        if callable(t):
            func = t; subitems = None
        else:
            subitems = t

    root = MenuItem(_id, text)
    if subitems is not None:
        for i in subitems:
            elem = build_menu_item(i)
            root.append_subitem( elem )
    else:
        root.func = func
    return root

class MenuItem:

    def __init__(self, id, text, *, func=None, items=None ):
        self.id = id
        self.text = text
        self.subitems = items or []
        self.func = func
        self.parent = None

    def append_subitem(self, subitem):
        self.subitems.append(subitem)
        subitem.parent = self

    def activate(self, update, ctx):
        if self.func is not None:
            return self.func(update, ctx)

class MenuHandler(Handler):
    def __init__(self, menu):
        super().__init__(self.callback)
        # logger.info( yaml.dump(menu) )
        self.menu = menu

    def check_update(self, update):
        try:
#            logger.info('check_update: UPDATE=\n'+yaml.dump(update) )
            if isinstance(update, Update) and update.effective_message:
                if update.callback_query:
                    return True
                    
                message = update.effective_message
                if (message.entities and message.entities[0].type == MessageEntity.BOT_COMMAND
                        and message.entities[0].offset == 0):
                    command = message.text[1:message.entities[0].length]

                    return command
                

            return None
        except:
            logger.exception('check_update')

    def keyboard_for_item(self, item):
        if item is None:
            return ReplyKeyboardRemove()
        kbd = [ [ '/'+i.id ] for i in item.subitems ]
        if item.parent is not None: 
            kbd.append( ['/cancel'] )
        return ReplyKeyboardMarkup(kbd, one_time_keyboard=True)
    
    def callback(self, update: Update, ctx: CallbackContext):
        try:
        
            global last_chats
            last_chats.put( update.effective_user.id, update.effective_user.first_name )

            citem = ctx.chat_data.get('current_item', self.menu)
            message = update.effective_message
            
            logger.info(f'callback called, citem={citem.id if citem is not None else None}, message.text={message.text}')

            if update.callback_query:
                ret = citem.activate(update, ctx)
                return

            newitem = None

            if message.entities and message.entities[0].type == MessageEntity.BOT_COMMAND: 
                command = message.text[message.entities[0].offset+1 : message.entities[0].length]
                logger.info(f'Command: {command}, citem={citem.id if citem is not None else "None"}')
                cmdfound = False
                if command == 'cancel':
                    newitem = citem.parent
                    if newitem is not None:
                        cmdfound = True
                else:
                    for i in citem.subitems:
                        if i.id == command:
                            newitem = i
                            cmdfound = True
                            break
                if not cmdfound:
                    message.reply_text( 
                        f'Unknown command. current menu is {citem.id}({citem.text})',
                        reply_markup=self.keyboard_for_item(citem) )
                    return
                    
                
                print_default_menu = True
                change_citem = True

                if newitem.func is not None:
                    change_citem = False
                    ret = newitem.activate(update, ctx) 

                    if ret is not None: 
                        print_default_menu = False
                        if ret:  change_citem = True; print_default_menu = True
                if change_citem:
                    citem = newitem                
                    ctx.chat_data['current_item'] = citem
                    
                if print_default_menu:
                    message.reply_text(
                        citem.text,
                        reply_markup=self.keyboard_for_item(citem) )
                    
            else:
                # just send to callback
                citem.activate(update, ctx)
                return

                    
        except:
            logger.exception('callback')

@restricted
def toggle(update, context):
    global mqtt_client
    message = update.effective_message
    cmd = message.text[1:message.entities[0].length]
    update.message.reply_text('Sending command "%s"' % cmd)
    logger.info("sending command "+cmd)
    global last_command_sender;  last_command_sender = update.effective_chat.id
    mqtt_client.publish('ledscreen/command', payload=cmd)

def set_brightness(val: int):
    global mqtt_client
    mqtt_client.publish('ledscreen/command', payload=f'br {val//10}')

@restricted
def brightness(update, context):
    logger.info('brightness')
    b10 = InlineKeyboardButton('10', callback_data='10')
    b50 = InlineKeyboardButton('50', callback_data='50')
    b255 = InlineKeyboardButton('255', callback_data='255')
    ikbd = InlineKeyboardMarkup.from_row([b10, b50, b255])

    global last_command_sender
    if update.callback_query:
        query = update.callback_query

        last_command_sender = update.effective_chat.id
        set_brightness( int(query.data) )
        query.edit_message_text(text="Selected option: {}".format(query.data), reply_markup=ikbd)
        query.answer()
    else:
        if update.message and update.message.text and update.message.text[0]!='/':
            last_command_sender = update.effective_chat.id
            set_brightnes( int(update.message.text) )
        logger.info( str(update.message.text) )
        update.message.reply_text('Set brightness:',  reply_markup=ikbd )
        
    return True


def get_id(update, ctx):
    user = update.message.from_user
    if not user:
        update.message.reply_text("Cannot get ID!" )
        return 
    update.message.reply_text("Your ID is "+str( user.id ) )
    
@restricted
def list_ids(update, ctx):
    global ids
    update.message.reply_text("Registered IDs are: "+str( ids ) )

@restricted  
def add_id(update, ctx):
    global ids
    if len(ids)==0:
        id = update.effective_user.id
    else:
        global last_chats
        logger.info( last_chats.items() )
        ikbd = InlineKeyboardMarkup.from_column( [ InlineKeyboardButton(f'{v} ({k})', callback_data=k ) for k,v in last_chats.items() ] )
    
        if update.callback_query:
            query = update.callback_query

            last_command_sender = update.effective_chat.id
            
            id = int( query.data )
            query.edit_message_text(text=f'Selected option: {id}' )#, reply_markup=ikbd)
            query.answer()
        else:
        
            update.message.reply_text('Select user to add:',  reply_markup=ikbd )
            return True
    
    logger.info('Adding ID %d to database' % id)
    if not id in ids:
        ids.add(id)
        save()
        update.effective_message.reply_text("Added new ID" )
    else:
        update.effective_message.reply_text("ID already present" )
    return True

@restricted
def del_id(update, ctx):
   
    global ids
    id = int( update.message.text.split()[1] )
    logger.info('Removing ID %d ' % id)
    if id in ids:
        ids.remove(id)
        update.message.reply_text("ID removed" )
    else:
        update.message.reply_text("ID not present" )

def echo(update, context):
    """Echo the user message."""
    update.message.reply_text(update.message.text,
        reply_markup=ReplyKeyboardRemove() )


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)

  

def mqtt_connect(mqtt_client, userdata, flags, rc):
    if rc==0:
        logger.info("Connected to MQTT")
        mqtt_client.subscribe("ledscreen/status")
        
    else:
        logger.warning('Failed to connect to MQTT: '+str(rc) )

def mqtt_message(mqtt_client, userdata, msg):
    
    topic = msg.topic
    payload = msg.payload
    try:
        payload = payload.decode()
    except:
        pass # not a bytes object
    
    logger.info(topic + " = " + str(payload) )

    if last_command_sender is not None:
        global bot
        bot.send_message(last_command_sender, f'Response is: "{payload}"' )
        
def setupMqtt():
    global mqtt_client
    mqtt_client = mqtt.Client("telegram"+str(datetime.datetime.now()) ) #, transport="websockets")
    mqtt_client.on_connect = mqtt_connect
    mqtt_client.on_message = mqtt_message
    if MQTT_SSL:
        mqtt_client.tls_set()
    mqtt_client.username_pw_set(MQTT_USER, MQTT_PASSWORD)
    mqtt_client.connect(MQTT_SERVER, MQTT_PORT, 60 )
    logger.info('Starting MQTT wait')
    mqtt_client.loop_start()
    #mqtt_client.loop_forever(timeout=10, max_packets=1)

def load():
    logger.info('Loading storage from '+storage_file)
    global storage, ids
    try:
        storage = yaml.load(open(storage_file), Loader=yaml.Loader)
        ids = storage['ids']
    except:
        logger.exception('load failed')
        ids = set()
        storage = { 'ids': ids }
        save()

def save():
    global storage, storage_file
    yaml.dump( storage, open(storage_file, 'w') )


def setupTelegram(webhook_url=None):
    """If webhook_url is not passed, run with long-polling."""
    
    load()

    global bot
    
    if webhook_url:
        bot = Bot(TOKEN)
        update_queue = Queue()
        dp = Dispatcher(bot, update_queue, use_context=True)
    else:
        updater = Updater(TOKEN, use_context=True)
        bot = updater.bot
        dp = updater.dispatcher

    #dp.add_handler(CommandHandler("start", start))
    
    menu = {'id': 'start', 'text': 'Welcome to menu for this bot! Select appropriate menu item', 'items':(
            {'id': 'ledmatrix', 'text': 'LED matrix control', 'items':(
                ('on',  'turn on',  toggle ),
                ('off', 'turn off', toggle ),
                ('br',  'brightness', brightness)
        ) },
        { 'id': 'admin', 'text': 'Admin funcs', 'items': (
            ('listids', 'List IDs', list_ids),
            ('userid', 'Show your ID', get_id),
            ('addid', 'Add IDs', add_id),
            ('delid', 'Delete ID', del_id),
        ) }
    ) }
    
    dp.add_handler( MenuHandler( build_menu(menu) ) )

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)
        
        
    # Add your handlers here
    logger.info('Starting telegram')
    if webhook_url:
        bot.set_webhook(webhook_url=webhook_url)
        thread = Thread(target=dp.start, name='dispatcher')
        thread.start()
        return update_queue, bot
    else:
        bot.set_webhook()  # Delete webhook
        updater.start_polling()
        #thread = Thread(target=updater.idle, name='updater')
        #thread.start


if __name__ == '__main__':
    setupMqtt()
    setupTelegram()